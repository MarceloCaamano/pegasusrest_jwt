﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PegasusREST.Models;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Web;
using System.Xml;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace PegasusREST.Controllers
{
    public class RecuperarFotosController : ApiController
    {
        public class Foto
        {
            public string FechaYHora { get; set; }
            public string url { get; set; }
			public string urlThumb { get; set; }
        }


        //public class PeriodoFotos
        //{
            //public string periodo { get; set; }
        //    public List<Foto> Fotos { get; set; }
        //}


        // GET api/test
        [Authorize]
        public HttpResponseMessage Post()
        {
            //HttpCookieCollection cookies = HttpContext.Current.Request.Cookies;
            //int cookieIndex = 0;
            bool cookieExists = true;
            CultureInfo dateCulture = new CultureInfo("en-US");  //When converting dates to strings, format is mm-dd-yy
            //for (int i = 0; i < cookies.Count; i++)
            //{
            //    string tempKey = cookies.GetKey(i);
            //    if (tempKey == "validUserCookie")
            //    {
            //        cookieIndex = i;
            //        if (cookies[cookieIndex].Value != null)
            //            cookieExists = true;
            //        break;
            //    }
            //}
            if (cookieExists)
            {
                //cookies[cookieIndex].Expires = DateTime.Now.AddMinutes(5);
                //HttpContext.Current.Response.SetCookie(cookies[cookieIndex]);
                string urlFotos = ConfigurationManager.AppSettings["IMG_URL"];
                using (PegasusEntities context = new PegasusEntities())
                {
                    var stream = HttpContext.Current.Request.InputStream;
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    string JSONbody = Encoding.UTF8.GetString(buffer);
                    var parseTree = JsonConvert.DeserializeObject<JObject>(JSONbody);
                    Int64 idEquipo = -999;
                    DateTime fechaDede = DateTime.Today;
					DateTime fechaHasta = fechaDede;
                    List<Foto> historico = new List<Foto>();
                    foreach (var prop in parseTree.Properties())
                    {
                        switch (prop.Name)
                        {
                            case "idEquipo":
                                idEquipo = Convert.ToInt64(prop.Value);
                                break;
                            case "fechaDesde":
								fechaDede = Convert.ToDateTime(prop.Value);
                            break;
							case "fechaHasta":
								fechaHasta = Convert.ToDateTime(prop.Value);
                            break;
                        }
                    }
					//Add a day to fechaHasta so query includes all data through the end of that day
					fechaHasta = fechaHasta.AddHours(24);
                    
                    
                    //Find all sensors for this equipment
                    var fotosQuery = from v in context.Imagenes
                                      where v.idEquipo == idEquipo
                                      && v.FechaHora >= fechaDede && v.FechaHora< fechaHasta
                                      select v;
                    if (fotosQuery.Count() == 0)
                    {
                        //EquipoDatos = null;
                    }
                    else
                    {
                        List<Imagenes> fotos = fotosQuery.OrderByDescending(s => s.FechaHora).ToList();
                        
                        foreach (Imagenes d in fotos)
                        {
                            Foto hist = new Foto();
                            hist.FechaYHora = d.FechaHora.ToString("yyyy-MM-dd HH:mm");
                            //if (d.Valor <= -999.9)
                            //    hist.valor = 
                            hist.url = urlFotos + d.NombreImg;
                            hist.urlThumb = urlFotos + d.NombreThu;
                            historico.Add(hist);
                        }

                    }
                    //if (EquipoDatos.Count() == 0)
                    //    EquipoDatos = null;
                    var response = Request.CreateResponse();
                    response.Content = new StringContent(JsonConvert.SerializeObject(historico), Encoding.UTF8, "application/json");
                    return response;
                }
            }
            else
            {
                var response = Request.CreateResponse();
                JsonMsg expiredMsg = new JsonMsg();
                expiredMsg.msg = "Sesion de usuario expirada, por favor inicie la sesión nuevamente.";
                expiredMsg.accion = "reconectar";
                response.Content = new StringContent(JsonConvert.SerializeObject(expiredMsg), Encoding.UTF8, "application/json");
                return response;
            }
        }

 
    }
}
