﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PegasusREST.Models;
using System.Data.Entity.Core.Objects;
using System.Web;
using System.Xml;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PegasusREST.Controllers
{
    public class RecuperarMaximosYMinimosAllController : ApiController
    {
        public class Valores
        {
            public string nombre { get; set; }
            public string valor { get; set; }
            public string fecha {get; set;}
        }

        public class SensorDatos
        {
            public string nombreSensor { get; set; }
            public string unidad { get; set; }
            public int Icono { get; set; }
            public List<Valores> valoresSensor { get; set; }
        }

        public class SensorIDOrder
        {
            public int sensorID { get; set; }
            public int importancia { get; set; }
            public int VarXEqID { get; set; }
        }

        // GET api/test
        [Authorize]
        public HttpResponseMessage Post()
        {
            bool cookieExists = true;
            /*HttpCookieCollection cookies = HttpContext.Current.Request.Cookies;
            int cookieIndex = 0;
            for (int i = 0; i < cookies.Count; i++)
            {
                string tempKey = cookies.GetKey(i);
                if (tempKey == "validUserCookie")
                {
                    cookieIndex = i;
                    if (cookies[cookieIndex].Value != null )
                        cookieExists = true;
                    break;
                 }
            }*/
            if (cookieExists)
            {
               //# cookies[cookieIndex].Expires = DateTime.Now.AddMinutes(5);
               //# HttpContext.Current.Response.SetCookie(cookies[cookieIndex]);
            using (PegasusEntities context = new PegasusEntities())
            {
                var stream = HttpContext.Current.Request.InputStream;
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                string JSONbody = Encoding.UTF8.GetString(buffer);
                var parseTree = JsonConvert.DeserializeObject<JObject>(JSONbody);
                Int64 idEquipo = -999;
                DateTime fechaDede = Convert.ToDateTime("1900-01-01");
                DateTime fechaHasta = Convert.ToDateTime("2100-12-31");
                foreach (var prop in parseTree.Properties())
                {
                    switch (prop.Name)
                    {
                        case "idEquipo":
                            idEquipo = Convert.ToInt64(prop.Value);
                            break;
                        case "fechaDesde":
                            fechaDede = Convert.ToDateTime(prop.Value);
                            break;
                        case "fechaHasta":
                            fechaHasta = Convert.ToDateTime(prop.Value);
                            break;
                    }
                }
                //Add a day to fechaHasta so query includes all data through the end of that day
                fechaHasta = fechaHasta.AddHours(24);
                List<SensorDatos> EquipoDatos = new List<SensorDatos>();
                //Find all sensors for this equipment
                var sensIDQuery = from v in context.VariablesXEquipos
                                where v.IdEquipos == idEquipo
                                && v.DadodeBaja == false
                                select v;
                if (sensIDQuery.Count() == 0)
                {
                    //EquipoDatos = null;
                }
                else
                {
                    List<VariablesXEquipos> sensID = sensIDQuery.ToList();
                    List<SensorIDOrder> sensorID = new List<SensorIDOrder>();
                    List<SensorIDOrder> orderedSensorID = new List<SensorIDOrder>();
                    foreach (VariablesXEquipos v in sensID)
                    {
                        SensorIDOrder temp = new SensorIDOrder();
                        temp.sensorID = v.IdVariables;
                        temp.VarXEqID = v.Id;
                        var impQuery = from t in context.Variables
                                       where t.Id == v.IdVariables
                                       select t;
                        if (impQuery.Count() > 0)
                        {
                            temp.importancia = (int)impQuery.First().Importancia;
                            sensorID.Add(temp);
                        }
                    }
                    orderedSensorID = sensorID.OrderBy(x => x.importancia).ToList();
                    //Get Sensor Names
                    
                    foreach (SensorIDOrder id in orderedSensorID)
                    {
                        var sensQuery = from v in context.Variables
                                        where v.Id == id.sensorID
                                        select v;
                        if (sensQuery.Count() > 0)
                        {
                            SensorDatos sens = new SensorDatos();
                            //assign sensor name
                            Variable thisSensor = sensQuery.First();
                            string unit = (from u in context.Unidades
                                           where u.Id == thisSensor.IdUnidades
                                           select u.Nombre).First();
                            sens.nombreSensor = thisSensor.Nombre;
                            sens.unidad = unit;
                            sens.Icono = (int)thisSensor.Icono;
                            //get format for value
                            string numFormat = "F" + thisSensor.Decimales.ToString();
                            try
                            {
                                //Get data for this sensor
                                var datQuery = from d in context.DatosEquipos
                                               where d.IdVariablesXEquipos == id.VarXEqID &&
                                               d.Fecha >= fechaDede && d.Fecha < fechaHasta
                                               && d.Valor > -999.9
                                               select d;

                                //int temp = datQuery.Count();
                                sens.valoresSensor = new List<Valores>();
                                if (datQuery.Any())
                                {
                                    if (sens.nombreSensor.Contains("Viento"))
                                    {
                                        int idSensSpeed = -1;
                                        int idSensDir = -1;
                                        if (sens.nombreSensor.Contains("Viento prom"))
                                        {
                                            //idSensSpeed = (from v in context.Variables
                                            //               where v.Nombre.Contains("Velocidad") && v.Nombre.Contains("Viento prom")
                                            //               select v.Id).First();
                                            //idSensDir = (from v in context.Variables
                                            //             where v.Nombre.Contains("Dir") && v.Nombre.Contains("Viento prom")
                                            //             select v.Id).First();

                                            //*******DO NOTHING - don't return prevailing wind************
                                        }
                                        else if (sens.nombreSensor.Contains("Viento r"))
                                        {
                                            idSensSpeed = (from v in context.Variables
                                                           where v.Nombre.Contains("Velocidad") && v.Nombre.Contains("Viento r")
                                                           select v.Id).First();
                                            //get format code for wind speed
                                            int dec = (from v in context.Variables
                                                       where v.Id == idSensSpeed
                                                       select v.Decimales).First();
                                            string windFormat = "F" + dec.ToString();

                                            idSensDir = (from v in context.Variables
                                                         where v.Nombre.Contains("Dir") && v.Nombre.Contains("Viento r")
                                                         select v.Id).First();
                                       //}
                                            int idXSpeed = (from x in context.VariablesXEquipos
                                                            where x.IdEquipos == idEquipo && x.IdVariables == idSensSpeed
                                                            select x.Id).First();

                                            int idXDir = (from x in context.VariablesXEquipos
                                                          where x.IdEquipos == idEquipo && x.IdVariables == idSensDir
                                                          select x.Id).First();
                                            var windSpeedQuery = from d in context.DatosEquipos
                                                                 where d.IdVariablesXEquipos == idXSpeed &&
                                                                 d.Fecha >= fechaDede && d.Fecha < fechaHasta
                                                                 && d.Valor > -999.9
                                                                 select d;
                                            var windDirQuery = from d in context.DatosEquipos
                                                               where d.IdVariablesXEquipos == idXDir &&
                                                               d.Fecha >= fechaDede && d.Fecha < fechaHasta
                                                                 && d.Valor > -999.9
                                                               select d;
                                            if (windSpeedQuery.Any())
                                            {
                                                List<DatosEquipos> windData = windSpeedQuery.ToList();
                                                List<double> windVal = (from d in windData select d.Valor).ToList();
                                                double maxWind = windVal.Max();
                                                DateTime maxWindDate = (from d in windData where d.Valor == maxWind select d.Fecha).First();
                                                Valores windSpeed = new Valores();
                                                windSpeed.nombre = "Max Anemómetro";
                                                windSpeed.valor = maxWind.ToString(windFormat);
                                                windSpeed.fecha = maxWindDate.ToString("yyyy-MM-dd HH:mm"); 
                                                Valores windDir = new Valores();
                                                windDir.nombre = "Veleta";
                                                if (windDirQuery.Any())
                                                {
                                                    List<DatosEquipos> dirData = windDirQuery.ToList();
                                                    var dateQuer = from d in dirData where d.Fecha == maxWindDate select d;
                                                    if (dateQuer.Count() > 0)
                                                    {
                                                        //Put in letter format
                                                        double windDirAngle = dateQuer.First().Valor;
                                                        if (windDirAngle >= 348.75 && windDirAngle < 11.25)
                                                            windDir.valor = "N";
                                                        else if (windDirAngle >= 11.25 && windDirAngle < 33.75)
                                                            windDir.valor = "NNE";
                                                        else if (windDirAngle >= 33.75 && windDirAngle < 56.25)
                                                            windDir.valor = "NE";
                                                        else if (windDirAngle >= 56.25 && windDirAngle < 78.75)
                                                            windDir.valor = "ENE";
                                                        else if (windDirAngle >= 78.75 && windDirAngle < 101.25)
                                                            windDir.valor = "E";
                                                        else if (windDirAngle >= 101.25 && windDirAngle < 123.75)
                                                            windDir.valor = "ESE";
                                                        else if (windDirAngle >= 123.75 && windDirAngle < 146.25)
                                                            windDir.valor = "SE";
                                                        else if (windDirAngle >= 146.25 && windDirAngle < 168.75)
                                                            windDir.valor = "SSE";
                                                        else if (windDirAngle >= 168.75 && windDirAngle < 191.25)
                                                            windDir.valor = "S";
                                                        else if (windDirAngle >= 191.25 && windDirAngle < 213.75)
                                                            windDir.valor = "SSO";
                                                        else if (windDirAngle >= 213.75 && windDirAngle < 236.25)
                                                            windDir.valor = "SO";
                                                        else if (windDirAngle >= 236.25 && windDirAngle < 258.75)
                                                            windDir.valor = "OSO";
                                                        else if (windDirAngle >= 258.75 && windDirAngle < 281.25)
                                                            windDir.valor = "O";
                                                        else if (windDirAngle >= 281.25 && windDirAngle < 303.75)
                                                            windDir.valor = "ONO";
                                                        else if (windDirAngle >= 303.75 && windDirAngle < 326.25)
                                                            windDir.valor = "NO";
                                                        else if (windDirAngle >= 326.25 && windDirAngle < 348.75)
                                                            windDir.valor = "NNO";
                                                        else
                                                            windDir.valor = "ERROR";
                                                        windDir.fecha = maxWindDate.ToString("yyyy-MM-dd HH:mm"); 
                                                    }
                                                    else
                                                        windDir.valor = "[]";
                                                }
                                                else
                                                    windDir.valor = "[]";
                                                sens.valoresSensor.Add(windSpeed);
                                                sens.valoresSensor.Add(windDir);
                                            }
                                        }
                                    }
                                    else 
                                    {
                                        //List of data
                                        List<DatosEquipos> datos = datQuery.ToList();
                                        List<double> datVal = (from d in datos select d.Valor).ToList();
                                        if (sens.nombreSensor.Contains("Precipitación") || sens.nombreSensor.Contains("Lluvia"))
                                        {
                                            Valores average = new Valores();
                                            average.nombre = "Acumulado";
                                            average.valor = datVal.Sum().ToString(numFormat);
                                            average.fecha = "[]";
                                            Valores maximum = new Valores();
                                            maximum.nombre = "Maxima lluvia en una hora";
                                            double maxVal = datVal.Max();
                                            DateTime maxDate = (from d in datos where d.Valor == maxVal select d.Fecha).First();
                                            maximum.fecha = maxDate.ToString("yyyy-MM-dd HH:mm");
                                            //find interval
                                            List<DatosEquipos> orderedDatos = (from d in datos orderby d.Fecha select d).ToList();
                                            List<TimeSpan> timeDiff = new List<TimeSpan>();
                                            for (int i = 1; i < orderedDatos.Count(); i++)
                                            {
                                                TimeSpan t = orderedDatos[i].Fecha - orderedDatos[i - 1].Fecha;
                                                timeDiff.Add(t);
                                            }
                                            TimeSpan td = timeDiff.Min();
                                            double hours = td.Hours + td.Minutes/60.0 + td.Seconds/3600.0;
                                            maximum.valor = (maxVal / hours).ToString(numFormat); //mm/hour
                                            sens.valoresSensor.Add(average);
                                            sens.valoresSensor.Add(maximum);
                                        }
                                        else
                                        {
                                            Valores minimum = new Valores();
                                            minimum.nombre = "Min";
                                            Valores maximum = new Valores();
                                            maximum.nombre = "Max";
                                            Valores average = new Valores();
                                            double minVal = datVal.Min();
                                            minimum.valor = minVal.ToString(numFormat);
                                            DateTime minDate = (from d in datos where d.Valor == minVal select d.Fecha).First();
                                            minimum.fecha = minDate.ToString("yyyy-MM-dd HH:mm");
                                            double maxVal = datVal.Max();
                                            maximum.valor = maxVal.ToString(numFormat);
                                            DateTime maxDate = (from d in datos where d.Valor == maxVal select d.Fecha).First();
                                            maximum.fecha = maxDate.ToString("yyyy-MM-dd HH:mm");
                                            average.nombre = "Promedio";
                                            average.valor = datVal.Average().ToString(numFormat);
                                            average.fecha = "[]";
                                            sens.valoresSensor.Add(minimum);
                                            sens.valoresSensor.Add(maximum);
                                            sens.valoresSensor.Add(average);
                                        }
                                    }
                                }
                                else
                                {
                                    //sens.valoresSensor = null;
                                }

                                EquipoDatos.Add(sens);
                            }
                            catch (System.Data.DataException excp)
                            {
                                var response2 = Request.CreateResponse();
                                JsonMsg tooMuchDataMsg = new JsonMsg();
                                tooMuchDataMsg.msg = "Se ha solicitado demasiada información.  Por favor acorte el periodo solicitado.";
                                tooMuchDataMsg.accion = "mostrar_msg";
                                response2.Content = new StringContent(JsonConvert.SerializeObject(tooMuchDataMsg), Encoding.UTF8, "application/json");
                                return response2;
                            }
                            catch (Exception ex)
                            {
                                 throw;
                            }
                        }
                    }

                }
                //if (EquipoDatos.Count() == 0)
                //    EquipoDatos = null;
                var response = Request.CreateResponse();
                response.Content = new StringContent(JsonConvert.SerializeObject(EquipoDatos), Encoding.UTF8, "application/json");
                return response;
            }
            }
            else
            {
                var response = Request.CreateResponse();
                JsonMsg expiredMsg = new JsonMsg();
                expiredMsg.msg = "Sesion de usuario expirada, por favor inicie la sesión nuevamente.";
                expiredMsg.accion = "reconectar";
                response.Content = new StringContent(JsonConvert.SerializeObject(expiredMsg), Encoding.UTF8, "application/json");
                return response;
            }
        }
    }
}
