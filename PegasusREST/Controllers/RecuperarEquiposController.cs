﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PegasusREST.Models;
using System.Data.Entity.Core.Objects;
using System.Web;
using System.Xml;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PegasusREST.Controllers
{

    public class ClientEquipo
    {
        public Int64 idEquipo {get; set;}
        public string descripcion {get; set;}
        public double lat {get; set;}
        public double lng {get; set;}
        public string NroSerie {get; set;}
        public string fechaAlta {get; set;}
        public List<Sensor> sensores {get; set;}
    }

    public class Sensor
    {
        public Int64 idSensor {get; set;}
        public string nombre {get; set;}
        public string unidad { get; set; }
        public int Icono { get; set; }
    }

    public class JsonMsg
    {
        public string msg { get; set; }
        public string accion { get; set; }
    }

    public class RecuperarEquiposController : ApiController
    {
        // GET api/test
        [Authorize]
        public HttpResponseMessage Post()
        {
            //#  HttpCookieCollection cookies = HttpContext.Current.Request.Cookies;
            //# int cookieIndex = 0;
            //# bool cookieExists = false;
            bool cookieExists = true;
            //# for (int i = 0; i < cookies.Count; i++)
            //#{
            //# string tempKey = cookies.GetKey(i);
            //#if (tempKey == "validUserCookie")
            //#{
            //#cookieIndex = i;
            //#if (cookies[cookieIndex].Value != null)
            //#cookieExists = true;
            //#break;
            //#}
            //#}

            if (cookieExists)
            {
                //# cookies[cookieIndex].Expires = DateTime.Now.AddMinutes(5);
                //# HttpContext.Current.Response.SetCookie(cookies[cookieIndex]);
                using (PegasusEntities context = new PegasusEntities())
                {
                    List<ClientEquipo> equips = new List<ClientEquipo>();
                    List<ClientEquipo> sortedEquips = new List<ClientEquipo>();
                    var stream = HttpContext.Current.Request.InputStream;
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    string JSONbody = Encoding.UTF8.GetString(buffer);
                    var parseTree = JsonConvert.DeserializeObject<JObject>(JSONbody);
                    Int64 idCliente = -999;
                    foreach (var prop in parseTree.Properties())
                    {
                        switch (prop.Name)
                        {
                            case "idCliente":
                                idCliente = Convert.ToInt64(prop.Value);
                                break;
                        }
                    }

                    //Get list of equipos ID that match client ID
                    var equipIDQuery = from u in context.UsuariosClimagroes
                                       where u.IdUsuario == idCliente
                                       select u.IdEquipos;
                    if (equipIDQuery.Count() == 0)
                    {
                        equips = null;
                    }
                    else
                    {
                        List<int?> equipID = equipIDQuery.ToList();

                        //Get information for each equipo in list
                        foreach (int eq in equipID)
                        {
                            var equipQuery = from e in context.Equipos
                                             where e.Id == eq &&  !e.Eliminado
                                             select e;
                            if (equipQuery.Count() > 0)
                            {
                                Equipos equip = equipQuery.First();

                                //add info from equip to new ClientEquipo
                                ClientEquipo newEq = new ClientEquipo();
                                newEq.idEquipo = equip.Id;
                                newEq.descripcion = equip.Nombre;
                                if (equip.LatGrados < 0)
                                    newEq.lat = Convert.ToDouble(equip.LatGrados) - Convert.ToDouble(equip.LatMinutos) / 60 - Convert.ToDouble(equip.LatSegundos) / 3600;
                                else
                                    newEq.lat = Convert.ToDouble(equip.LatGrados) + Convert.ToDouble(equip.LatMinutos) / 60 + Convert.ToDouble(equip.LatSegundos) / 3600;
                                if (equip.LongGrados < 0)
                                    newEq.lng = Convert.ToDouble(equip.LongGrados) - Convert.ToDouble(equip.LongMinutos) / 60 - Convert.ToDouble(equip.LongSegundos) / 3600;
                                else
                                    newEq.lng = Convert.ToDouble(equip.LongGrados) + Convert.ToDouble(equip.LongMinutos) / 60 + Convert.ToDouble(equip.LongSegundos) / 3600;
                                newEq.NroSerie = equip.NroSerie.ToString();
                                newEq.fechaAlta = equip.FechaRegistracion.ToString("dd/MM/yyyy HH:mm:ss");

                                //Get Sensor IDs that match this equipo
                                var EqSens = from e in context.VariablesXEquipos
                                             where e.IdEquipos == eq && e.DadodeBaja == false
                                             select e.IdVariables;
                                if (EqSens.Count() == 0)
                                {
                                    newEq.sensores = null;
                                }
                                else
                                {
                                    List<int> sensorIDTemp = EqSens.ToList();
                                    List<Variable> sensorsToSort = new List<Variable>();
                                    List<Variable> sortedSensors = new List<Variable>();
                                    List<Sensor> sensores = new List<Sensor>();
                                    //get sensor names and order
                                    foreach (int sens in sensorIDTemp)
                                    {
                                        var vari = (from v in context.Variables
                                                    where v.Id == sens
                                                    select v);
                                        if (vari.Count() > 0)
                                            sensorsToSort.Add(vari.First());
                                    }
                                    sortedSensors = sensorsToSort.OrderBy(x => x.Importancia).ToList();

                                    //Get information for each sensor
                                    foreach (Variable sens in sortedSensors)
                                    {
                                        Sensor thisSens = new Sensor();
                                        thisSens.idSensor = sens.Id;
                                        thisSens.Icono = (int)sens.Icono;
                                        //Get units for this sensor
                                        string unit = (from u in context.Unidades
                                                       where u.Id == sens.IdUnidades
                                                       select u.Nombre).First();
                                        thisSens.nombre = sens.Nombre;
                                        thisSens.unidad = unit;
                                        sensores.Add(thisSens);
                                    }
                                    newEq.sensores = sensores;
                                }
                                //add to list
                                equips.Add(newEq);
                            }

                        }
                        sortedEquips = equips.OrderBy(x => x.descripcion).ToList();
                    }


                    var response = Request.CreateResponse();
                    response.Content = new StringContent(JsonConvert.SerializeObject(sortedEquips), Encoding.UTF8, "application/json");
                    return response;
                }
            }
            else
            {
                var response = Request.CreateResponse();
                JsonMsg expiredMsg = new JsonMsg();
                expiredMsg.msg = "Sesion de usuario expirada, por favor inicie la sesión nuevamente.";
                expiredMsg.accion = "reconectar";
                response.Content = new StringContent(JsonConvert.SerializeObject(expiredMsg), Encoding.UTF8, "application/json");
                return response;
            }
        }
    }               
}