﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PegasusREST.Models;
using System.Data.Entity.Core.Objects;
using System.Web;
using System.Xml;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace PegasusREST.Controllers
{
    public class RecuperarMaximosYMinimosController : ApiController
    {
        public class Valores
        {
            public string nombre { get; set; }
            public string valor { get; set; }
        }

        //public class SensorDatos
        //{
        //    public string nombreSensor { get; set; }
        //    public string unidad { get; set; }
        //    public int Icono { get; set; }
        //    public List<Valores> valoresSensor { get; set; }
        //}

        public class PeriodoDatos
        {
            public string periodo { get; set; }
            public List<Valores> Datos { get; set; }
        }

        public class SensorIDOrder
        {
            public int sensorID { get; set; }
            public int importancia { get; set; }
            public int VarXEqID { get; set; }
        }

        // GET api/test
        [Authorize]
        public HttpResponseMessage Post()
        {
            bool cookieExists = true;
            CultureInfo dateCulture = new CultureInfo("en-US");  //When converting dates to strings, format is mm-dd-yy
            /*
            HttpCookieCollection cookies = HttpContext.Current.Request.Cookies;
            int cookieIndex = 0;
            
            for (int i = 0; i < cookies.Count; i++)
            {
                string tempKey = cookies.GetKey(i);
                if (tempKey == "validUserCookie")
                {
                    cookieIndex = i;
                    if (cookies[cookieIndex].Value != null)
                        cookieExists = true;
                    break;
                }
            }*/
            if (cookieExists)
            {
               //# cookies[cookieIndex].Expires = DateTime.Now.AddMinutes(5);
               //# HttpContext.Current.Response.SetCookie(cookies[cookieIndex]);
                using (PegasusEntities context = new PegasusEntities())
                {
                    var stream = HttpContext.Current.Request.InputStream;
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    string JSONbody = Encoding.UTF8.GetString(buffer);
                    var parseTree = JsonConvert.DeserializeObject<JObject>(JSONbody);
                    Int64 idEquipo = -999;
                    int tipo = 0;
                    foreach (var prop in parseTree.Properties())
                    {
                        switch (prop.Name)
                        {
                            case "idEquipo":
                                idEquipo = Convert.ToInt64(prop.Value);
                                break;
                            case "tipoDeConsulta":
                                tipo = Convert.ToInt32(prop.Value);
                                break;
                        }
                    }
                    //Get date values:
                    DateTime startToday = DateTime.Now.Date;  //<---USE THIS
                    //DateTime startToday = Convert.ToDateTime("6-15-2010 15:45").Date;  //<---JUST FOR TESTING
                    DateTime endToday = startToday.AddDays(1);
                    DateTime startYesterday = startToday.AddDays(-1);
                    DateTime thisMonth = Convert.ToDateTime(startToday.Month.ToString() + "-1-" + startToday.Year.ToString(), dateCulture);
                    DateTime lastMonth = Convert.ToDateTime(startToday.AddMonths(-1).Month.ToString() + "-1-" + startToday.AddMonths(-1).Year.ToString(), dateCulture);
                    List<DateTime> startTimes = new List<DateTime>{startToday, startYesterday, thisMonth, lastMonth};
                    List<DateTime> endTimes = new List<DateTime> { endToday, startToday, endToday, thisMonth };
                    List<string> periodNames = new List<string> {"Hoy","Ayer","Mes actual","Mes pasado"};
                    List<string> dateNames = new List<string> {"Hoy "+startToday.ToString("dd/MM/yyyy"),"Ayer "+startYesterday.ToString("dd/MM/yyyy"),
                                                                "Mes de "+thisMonth.ToString("MMMM"), "Mes de "+lastMonth.ToString("MMMM")};
                    List<Valores> EquipoDatos = new List<Valores>();
                    PeriodoDatos periodo = new PeriodoDatos();
                    //Find all sensors for this equipment
                    var sensIDQuery = from v in context.VariablesXEquipos
                                      where v.IdEquipos == idEquipo
                                      && v.DadodeBaja == false
                                      select v;
                    if (sensIDQuery.Count() == 0 || tipo<1 || tipo>4)
                    {
                        //EquipoDatos = null;
                    }
                    else
                    {
                        List<VariablesXEquipos> sensID = sensIDQuery.ToList();
                        List<SensorIDOrder> sensorID = new List<SensorIDOrder>();
                        List<SensorIDOrder> orderedSensorID = new List<SensorIDOrder>();
                        foreach (VariablesXEquipos v in sensID)
                        {
                            //Filter out only sensors we want
                            string name = (from s in context.Variables where s.Id == v.IdVariables select s.Nombre).First();
                            if (name == "Presión Atmosférica" || name == "Lluvia Caida" || name == "Velocidad de Viento ráfaga" //|| name == "Dirección de  Viento ráfaga"
                               || name == "Temperatura de Aire exterior" || name == "Humedad de Aire exterior" || name == "Evapotranspiración diaria")
                            {
                                SensorIDOrder temp = new SensorIDOrder();
                                temp.sensorID = v.IdVariables;
                                temp.VarXEqID = v.Id;
                                var impQuery = from t in context.Variables
                                               where t.Id == v.IdVariables
                                               select t;
                                if (impQuery.Count() > 0)
                                {
                                    temp.importancia = (int)impQuery.First().Importancia;
                                    sensorID.Add(temp);
                                }
                            }
                        }
                        orderedSensorID = sensorID.OrderBy(x => x.importancia).ToList();
                        //Get Sensor Names

                        //for (int i = 0; i < startTimes.Count(); i++)
                        //{
                        
                            int i = tipo -1;
                            periodo.periodo = "Valores de " + dateNames[i];
                            //List<SensorDatos> PeriodoSensores = new List<SensorDatos>();
                            DateTime start = startTimes[i];
                            DateTime end = endTimes[i];
                            //Format date depending on time period
                            string dateFormat = "";
                            if (periodNames[i] == "Hoy" || periodNames[i] == "Ayer")
                                dateFormat = "' a las 'HH:mm";
                            else
                                dateFormat = "' el dia 'dd/MM/yyyy' a las 'HH:mm";
                            foreach (SensorIDOrder id in orderedSensorID)
                            {
                                var sensQuery = from v in context.Variables
                                                where v.Id == id.sensorID
                                                select v;
                                if (sensQuery.Count() > 0)
                                {
                                    //assign sensor name
                                    Variable thisSensor = sensQuery.First();
                                    string unit = (from u in context.Unidades
                                                   where u.Id == thisSensor.IdUnidades
                                                   select u.Nombre).First();
                                    if (unit != "%" && unit != "°C")
                                        unit = " " + unit;
                                    string sensorName = thisSensor.Nombre;
                                    //get format for value
                                    string numFormat = "F" + thisSensor.Decimales.ToString();
                                    try
                                    {
                                        //Get data for this sensor
                                        var datQuery = from d in context.DatosEquipos
                                                       where d.IdVariablesXEquipos == id.VarXEqID &&
                                                       d.Fecha >= start && d.Fecha < end
                                                       && d.Valor > -999.9
                                                       select d;

                                        //int temp = datQuery.Count();
                                        if (datQuery.Any())
                                        {
                                            if (sensorName.Contains("Viento"))
                                            {
                                                int idSensSpeed = -1;
                                                int idSensDir = -1;
                                                if (sensorName.Contains("Viento prom"))
                                                {
                                                    //idSensSpeed = (from v in context.Variables
                                                    //               where v.Nombre.Contains("Velocidad") && v.Nombre.Contains("Viento prom")
                                                    //               select v.Id).First();
                                                    //idSensDir = (from v in context.Variables
                                                    //             where v.Nombre.Contains("Dir") && v.Nombre.Contains("Viento prom")
                                                    //             select v.Id).First();

                                                    //*******DO NOTHING - don't return prevailing wind************
                                                }
                                                else if (sensorName.Contains("Viento r"))
                                                {
                                                    idSensSpeed = (from v in context.Variables
                                                                   where v.Nombre.Contains("Velocidad") && v.Nombre.Contains("Viento r")
                                                                   select v.Id).First();
                                                    //get format code for wind speed
                                                    Variable speed = (from v in context.Variables
                                                                      where v.Id == idSensSpeed
                                                                      select v).First();
                                                    int dec = speed.Decimales;
                                                    string speedUnit = " " + speed.Unidade.Nombre;
                                                    string speedName = speed.Nombre;
                                                    string windFormat = "F" + dec.ToString();

                                                    Variable dir = (from v in context.Variables
                                                                    where v.Nombre.Contains("Dir") && v.Nombre.Contains("Viento r")
                                                                    select v).First();
                                                    idSensDir = dir.Id;
                                                    string dirName = dir.Nombre;
                                                    //}
                                                    int idXSpeed = (from x in context.VariablesXEquipos
                                                                    where x.IdEquipos == idEquipo && x.IdVariables == idSensSpeed
                                                                    select x.Id).First();

                                                    int idXDir = (from x in context.VariablesXEquipos
                                                                  where x.IdEquipos == idEquipo && x.IdVariables == idSensDir
                                                                  select x.Id).First();
                                                    var windSpeedQuery = from d in context.DatosEquipos
                                                                         where d.IdVariablesXEquipos == idXSpeed &&
                                                                         d.Fecha >= start && d.Fecha < end
                                                                         && d.Valor > -999.9
                                                                         select d;
                                                    var windDirQuery = from d in context.DatosEquipos
                                                                       where d.IdVariablesXEquipos == idXDir &&
                                                                       d.Fecha >= start && d.Fecha < end
                                                                         && d.Valor > -999.9
                                                                       select d;
                                                    if (windSpeedQuery.Any())
                                                    {
                                                        List<DatosEquipos> windData = windSpeedQuery.ToList();
                                                        List<double> windVal = (from d in windData select d.Valor).ToList();
                                                        double maxWind = windVal.Max();
                                                        DateTime maxWindDate = (from d in windData where d.Valor == maxWind select d.Fecha).First();
                                                        Valores windSpeed = new Valores();
                                                        windSpeed.nombre = "Max " + speedName;
                                                        windSpeed.valor = maxWind.ToString(windFormat) + speedUnit + maxWindDate.ToString(dateFormat);
                                                        //windSpeed.fecha = maxWindDate.ToString(dateFormat);
                                                        Valores windDir = new Valores();
                                                        windDir.nombre = "Max " + dirName;
                                                        if (windDirQuery.Any())
                                                        {
                                                            List<DatosEquipos> dirData = windDirQuery.ToList();
                                                            var dateQuer = from d in dirData where d.Fecha == maxWindDate select d;
                                                            if (dateQuer.Count() > 0)
                                                            {
                                                                //Put in letter format
                                                                double windDirAngle = dateQuer.First().Valor;
                                                                windDir.valor = GetWindDirLetters(windDirAngle) + maxWindDate.ToString(dateFormat);
                                                                //windDir.fecha = maxWindDate.ToString(dateFormat);
                                                            }
                                                            else
                                                                windDir.valor = "[]";
                                                        }
                                                        else
                                                            windDir.valor = "[]";
                                                        EquipoDatos.Add(windSpeed);
                                                        EquipoDatos.Add(windDir);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //List of data
                                                List<DatosEquipos> datos = datQuery.ToList();
                                                List<double> datVal = (from d in datos select d.Valor).ToList();
                                                if (sensorName.Contains("Precipitación") || sensorName.Contains("Lluvia"))
                                                {
                                                    Valores average = new Valores();
                                                    average.nombre = "Acumulado "+sensorName;
                                                    average.valor = datVal.Sum().ToString(numFormat)+unit;
                                                    //average.fecha = "[]";
                                                    Valores maximum = new Valores();
                                                    maximum.nombre = "Maxima lluvia en una hora";
                                                    double maxVal = datVal.Max();
                                                    DateTime maxDate = (from d in datos where d.Valor == maxVal select d.Fecha).First();
                                                    //maximum.fecha = maxDate.ToString(dateFormat);
                                                    //find interval
                                                    List<DatosEquipos> orderedDatos = (from d in datos orderby d.Fecha select d).ToList();
                                                    List<TimeSpan> timeDiff = new List<TimeSpan>();
                                                    for (int j = 1; j < orderedDatos.Count(); j++)
                                                    {
                                                        TimeSpan t = orderedDatos[j].Fecha - orderedDatos[j - 1].Fecha;
                                                        timeDiff.Add(t);
                                                    }
                                                    TimeSpan td = timeDiff.Min();
                                                    double hours = td.Hours + td.Minutes / 60.0 + td.Seconds / 3600.0;
                                                    maximum.valor = (maxVal / hours).ToString(numFormat) + unit + "/hora" + maxDate.ToString(dateFormat); 
                                                    EquipoDatos.Add(average);
                                                    EquipoDatos.Add(maximum);
                                                }
                                                else if (sensorName.Contains("Evap") && (tipo == 1 || tipo ==2))  //Only one value if day range is just one day
                                                {
                                                    Valores evapDay = new Valores();
                                                    evapDay.nombre = sensorName;
                                                    evapDay.valor = datVal.First().ToString(numFormat) + unit + datos.First().Fecha.ToString(dateFormat);
                                                    EquipoDatos.Add(evapDay);
                                                }
                                                else
                                                {
                                                    Valores minimum = new Valores();
                                                    minimum.nombre = "Min "+sensorName;
                                                    Valores maximum = new Valores();
                                                    maximum.nombre = "Max "+sensorName;
                                                    Valores average = new Valores();
                                                    average.nombre = "Promedio " + sensorName;
                                                    double minVal = datVal.Min();
                                                    DateTime minDate = (from d in datos where d.Valor == minVal select d.Fecha).First();
                                                    minimum.valor = minVal.ToString(numFormat) +  unit + minDate.ToString(dateFormat);
                                                    //minimum.fecha = minDate.ToString(dateFormat);
                                                    double maxVal = datVal.Max();
                                                    DateTime maxDate = (from d in datos where d.Valor == maxVal select d.Fecha).First();
                                                    maximum.valor = maxVal.ToString(numFormat) +  unit + maxDate.ToString(dateFormat);
                                                    //maximum.fecha = maxDate.ToString(dateFormat);
                                                    average.valor = datVal.Average().ToString(numFormat)+unit;
                                                    //average.fecha = "[]";
                                                    EquipoDatos.Add(minimum);
                                                    EquipoDatos.Add(maximum);
                                                    EquipoDatos.Add(average);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //sens.valoresSensor = null;
                                        }

                                    }
                                    catch (System.Data.DataException excp)
                                    {
                                        var response2 = Request.CreateResponse();
                                        JsonMsg tooMuchDataMsg = new JsonMsg();
                                        tooMuchDataMsg.msg = "Se ha solicitado demasiada información.  Por favor acorte el periodo solicitado.";
                                        tooMuchDataMsg.accion = "mostrar_msg";
                                        response2.Content = new StringContent(JsonConvert.SerializeObject(tooMuchDataMsg), Encoding.UTF8, "application/json");
                                        return response2;
                                    }
                                    catch (Exception ex)
                                    {
                                        throw;
                                    }
                                }
                            } //Sensor loop
                            //thisPeriod.Datos = PeriodoSensores;
                           // EquipoDatos.Add(thisPeriod);
                            periodo.Datos = EquipoDatos;
                        //}//Period loop

                    }
                    //if (EquipoDatos.Count() == 0)
                    //    EquipoDatos = null;
                    var response = Request.CreateResponse();
                    response.Content = new StringContent(JsonConvert.SerializeObject(periodo), Encoding.UTF8, "application/json");
                    return response;
                }
            }
            else
            {
                var response = Request.CreateResponse();
                JsonMsg expiredMsg = new JsonMsg();
                expiredMsg.msg = "Sesion de usuario expirada, por favor inicie la sesión nuevamente.";
                expiredMsg.accion = "reconectar";
                response.Content = new StringContent(JsonConvert.SerializeObject(expiredMsg), Encoding.UTF8, "application/json");
                return response;
            }
        }

        private string GetWindDirLetters(double windDirAngle)
        {
            string windDir = "";
            if (windDirAngle >= 348.75 && windDirAngle < 11.25)
                windDir = "N";
            else if (windDirAngle >= 11.25 && windDirAngle < 33.75)
                windDir = "NNE";
            else if (windDirAngle >= 33.75 && windDirAngle < 56.25)
                windDir = "NE";
            else if (windDirAngle >= 56.25 && windDirAngle < 78.75)
                windDir = "ENE";
            else if (windDirAngle >= 78.75 && windDirAngle < 101.25)
                windDir = "E";
            else if (windDirAngle >= 101.25 && windDirAngle < 123.75)
                windDir = "ESE";
            else if (windDirAngle >= 123.75 && windDirAngle < 146.25)
                windDir = "SE";
            else if (windDirAngle >= 146.25 && windDirAngle < 168.75)
                windDir = "SSE";
            else if (windDirAngle >= 168.75 && windDirAngle < 191.25)
                windDir = "S";
            else if (windDirAngle >= 191.25 && windDirAngle < 213.75)
                windDir = "SSO";
            else if (windDirAngle >= 213.75 && windDirAngle < 236.25)
                windDir = "SO";
            else if (windDirAngle >= 236.25 && windDirAngle < 258.75)
                windDir = "OSO";
            else if (windDirAngle >= 258.75 && windDirAngle < 281.25)
                windDir = "O";
            else if (windDirAngle >= 281.25 && windDirAngle < 303.75)
                windDir = "ONO";
            else if (windDirAngle >= 303.75 && windDirAngle < 326.25)
                windDir = "NO";
            else if (windDirAngle >= 326.25 && windDirAngle < 348.75)
                windDir = "NNO";
            else
                windDir = "-";
            return windDir;
        }
    }
}
