﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PegasusREST.Models;
using System.Data.Entity.Core.Objects;
using System.Web;
using System.Xml;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PegasusREST.Controllers
{
    public class Historicos
    {
        public string fecha {get; set;}
        public double valor {get; set;}
    }

    public class RecuperarHistoricosDeEquipoPorSensorController : ApiController
    {
        // GET api/test
        [Authorize]
        public HttpResponseMessage Post()
        {
            /*HttpCookieCollection cookies = HttpContext.Current.Request.Cookies;
            int cookieIndex = 0;
            bool cookieExists = false;
            for (int i = 0; i < cookies.Count; i++)
            {
                string tempKey = cookies.GetKey(i);
                if (tempKey == "validUserCookie")
                {
                    cookieIndex = i;
                    if (cookies[cookieIndex].Value != null)
                        cookieExists = true;
                    break;
                 }
            }*/
            bool cookieExists = true;
            if (cookieExists)
            {
                //# cookies[cookieIndex].Expires = DateTime.Now.AddMinutes(5);
                //# HttpContext.Current.Response.SetCookie(cookies[cookieIndex]);
            using (PegasusEntities context = new PegasusEntities())
            {
                var stream = HttpContext.Current.Request.InputStream;
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                string JSONbody = Encoding.UTF8.GetString(buffer);
                var parseTree = JsonConvert.DeserializeObject<JObject>(JSONbody);
                Int64 idEquipo = -999;
                Int64 idSensor = -999;
                DateTime fechaDede = Convert.ToDateTime("1900-01-01");
                DateTime fechaHasta= Convert.ToDateTime("2100-12-31");
                foreach (var prop in parseTree.Properties())
                {
                    switch (prop.Name)
                    {
                        case "idEquipo":
                            idEquipo = Convert.ToInt64(prop.Value);
                            break;
                        case "idSensor":
                            idSensor = Convert.ToInt64(prop.Value);
                            break;
                        case "fechaDesde":
                            fechaDede = Convert.ToDateTime(prop.Value);
                            break;
                        case "fechaHasta":
                            fechaHasta = Convert.ToDateTime(prop.Value);
                            break;
                    }
                }
                //Add a day to fechaHasta so query includes all data through the end of that day
                fechaHasta = fechaHasta.AddHours(24);
                List<Historicos> HistoricosDeEquipoPorSensor = new List<Historicos>();
                //List of id's of variableXEquipo
                var VarEqQuery = from v in context.VariablesXEquipos
                                 where v.IdEquipos == idEquipo && v.IdVariables == idSensor 
                                 && v.DadodeBaja == false
                                 select v.Id;
                if (VarEqQuery.Count() == 0)
                {
                    //HistoricosDeEquipoPorSensor = null;
                }
                else
                {
                    //get format code for this variable
                    Variable thisVar = (from v in context.Variables
                                        where v.Id == idSensor
                                        select v).First();
                    int numDec = thisVar.Decimales;
                    string name = thisVar.Nombre;
                    string numFormat = "F" + numDec.ToString();
                    int varEq = VarEqQuery.First();
                    try
                    {
                        var dataQuery = from d in context.DatosEquipos
                                        where d.IdVariablesXEquipos == varEq &&
                                        d.Fecha >= fechaDede && d.Fecha < fechaHasta
                                        select d;

                        if (dataQuery.Count() == 0)
                        {
                            //HistoricosDeEquipoPorSensor = null;
                        }
                        else
                        {
                            List<DatosEquipos> datos = dataQuery.ToList();
                            foreach (DatosEquipos d in datos)
                            {
                                Historicos hist = new Historicos();
                                hist.fecha = d.Fecha.ToString("yyyy-MM-dd HH:mm");
                                //if (d.Valor <= -999.9)
                                //    hist.valor = 
                                hist.valor = Math.Round(d.Valor,numDec); 
                                HistoricosDeEquipoPorSensor.Add(hist);
                            }
                        }
                    }
                    catch (System.Data.DataException excp)
                    {
                        var response = Request.CreateResponse();
                        JsonMsg tooMuchDataMsg = new JsonMsg();
                        tooMuchDataMsg.msg = "Se ha solicitado demasiada información.  Por favor acorte el periodo solicitado.";
                        tooMuchDataMsg.accion = "mostrar_msg";
                        response.Content = new StringContent(JsonConvert.SerializeObject(tooMuchDataMsg), Encoding.UTF8, "application/json");
                        return response;
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }

                var response2 = Request.CreateResponse();
                response2.Content = new StringContent(JsonConvert.SerializeObject(HistoricosDeEquipoPorSensor), Encoding.UTF8, "application/json");
                return response2;
            }
            }
            else
            {
                var response = Request.CreateResponse();
                JsonMsg expiredMsg = new JsonMsg();
                expiredMsg.msg = "Sesion de usuario expirada, por favor inicie la sesión nuevamente.";
                expiredMsg.accion = "reconectar";
                response.Content = new StringContent(JsonConvert.SerializeObject(expiredMsg), Encoding.UTF8, "application/json");
                return response;
            }
        }
    }
}
