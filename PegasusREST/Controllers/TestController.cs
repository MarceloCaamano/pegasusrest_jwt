﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PegasusREST.Models;
using System.Data.Objects;
using System.Web;
using System.Xml;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PegasusREST.Controllers
{
    public class TestController : ApiController
    {
        // GET api/test
        public string Post()
        {
            using (SAT3_testEntities context = new SAT3_testEntities())
            {
                // Jill, this test function shows how to read the body from the message you send from RESTclient
                // the message will come in JSON format
                // The message for this controller corresponds to the AutenticarUsuario function parameters (see word document with methods))
                var stream = HttpContext.Current.Request.InputStream;
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                string JSONbody = Encoding.UTF8.GetString(buffer);
                var parseTree = JsonConvert.DeserializeObject<JObject>(JSONbody);
                string nombreDeUsuario, clave;
                foreach (var prop in parseTree.Properties())
                {
                    switch (prop.Name)
                    {
                        case "nombreDeUsuario":
                            nombreDeUsuario = prop.Value.ToString();
                            break;
                        case "clave":
                            clave = prop.Value.ToString();
                            break;
                    }
                }

                // Running the debugger I verified that the message is read properly
                //This response has nothing to do with what the function should do, it is just to see something back on the client
                List<Variable> l = context.Variable.ToList();
                return JsonConvert.SerializeObject(l);
            }
            
        }
    }
}
