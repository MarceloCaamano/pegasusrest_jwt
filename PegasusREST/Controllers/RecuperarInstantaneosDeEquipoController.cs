﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PegasusREST.Models;
using System.Data.Entity.Core.Objects;
using System.Web;
using System.Xml;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Data.Entity.Infrastructure; //Marcelo
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using System.Text.RegularExpressions;

namespace PegasusREST.Controllers
{
    public class EquipoInst
    {
        public string fechaUltimaActualizacionDatos {get; set;}
        public List<SensoresDatos> datosSensores {get; set;}
    }

    public class SensoresDatos
    {
        public string nombreSensor {get; set;}
        public string valor {get;set;}
        public string unidad { get; set; }
        public bool alarma {get; set;}
        public int Icono { get; set; }
    }

    public class SensorIDOrder
    {
        public int sensorID { get; set; }
        public int importancia { get; set; }
    }

    public class T_Calulos
    {
        public string Fecha { get; set; }
        public int Numero { get; set; }
        public string Variables { get; set; }
        public string Valor { get; set; }
    }


    public class RecuperarInstantaneosDeEquipoController : ApiController
    {
        // GET api/test
        [Authorize]

        public HttpResponseMessage Post()
        {

            bool cookieExists = true;
            bool humedad = false;
            bool temperatura = false;
            /*
            HttpCookieCollection cookies = HttpContext.Current.Request.Cookies;
            int cookieIndex = 0;
            for (int i = 0; i < cookies.Count; i++)
            {
                string tempKey = cookies.GetKey(i);
                if (tempKey == "validUserCookie")
                {
                    cookieIndex = i;
                    if (cookies[cookieIndex].Value != null)
                        cookieExists = true;
                    break;
                 }
            }*/
            if (cookieExists)
            {
                //# cookies[cookieIndex].Expires = DateTime.Now.AddMinutes(5);
                //# HttpContext.Current.Response.SetCookie(cookies[cookieIndex]);
            using (PegasusEntities context = new PegasusEntities())
            {
                EquipoInst equip = new EquipoInst();

                var stream = HttpContext.Current.Request.InputStream;
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                string JSONbody = Encoding.UTF8.GetString(buffer);
                var parseTree = JsonConvert.DeserializeObject<JObject>(JSONbody);
                Int64 idEquipo = -999;


                foreach (var prop in parseTree.Properties())
                {
                    switch (prop.Name)
                    {
                        case "idEquipo":
                            idEquipo = Convert.ToInt64(prop.Value);
                            break;

                    }
                }

                var varEqQuery = from v in context.VariablesXEquipos
                                 where v.IdEquipos == idEquipo && v.DadodeBaja == false
                                 select v;
                if (varEqQuery.Count() == 0)
                {
                    equip = null;
                }
                else
                {
                    //Get date for yesterday to use for evapotranspiration
                    DateTime stToday = DateTime.Now.Date;
                    //DateTime stToday =Convert.ToDateTime("8-4-2014 5:15"); //For testing
                    DateTime stYest = stToday.AddDays(-1);
                    List<VariablesXEquipos> varEquip = varEqQuery.ToList();
                    List<SensorIDOrder> sensorID = new List<SensorIDOrder>();
                    List<SensorIDOrder> orderedSensorID = new List<SensorIDOrder>();
                    foreach (var v in varEquip)
                    {
                        SensorIDOrder temp = new SensorIDOrder();
                        temp.sensorID = v.IdVariables;
                        var impQuery = from t in context.Variables
                                       where t.Id == v.IdVariables
                                       select t;
                        if (impQuery.Count() > 0)
                        {
                            temp.importancia = (int)impQuery.First().Importancia;
                            sensorID.Add(temp);
                        }
                    }
                    orderedSensorID = sensorID.OrderBy(x => x.importancia).ToList();
                    List<DateTime> fechas = new List<DateTime>();
                    List<SensoresDatos> sensores = new List<SensoresDatos>();
                    foreach (SensorIDOrder v in orderedSensorID)
                    {
                        SensoresDatos sens = new SensoresDatos();

                        //Find sensor
                        var vari = from va in context.Variables
                                   where va.Id == v.sensorID
                                   select va;
                        if (vari.Count() == 0)
                        {
                            sens = null;
                        }
                        else
                        {
                            Variable var = vari.First();
                            string numFormat = "F" + var.Decimales.ToString();
                            //Get units for this sensor
                            string unit = (from u in context.Unidades
                                           where u.Id == var.IdUnidades
                                           select u.Nombre).First();
                            sens.nombreSensor = var.Nombre;
                            sens.unidad = unit;
                            sens.Icono = (int)var.Icono;
                            ////Check for alarm
                            //var alarm = from a in context.AlarmasXVariables
                            //            where a.IdVariables == v.sensorID
                            //            select a;
                            //if (alarm.Count() == 0)
                            //    sens.alarma = false;
                            //else
                            //    sens.alarma = true;
                            sens.alarma = false;

                            //Find idVarxEquipos for this sensor
                            int idVarXEq = (from x in context.VariablesXEquipos
                                            where x.IdEquipos == idEquipo && x.IdVariables == v.sensorID
                                            select x.Id).First();

                            if (sens.nombreSensor.Contains("Evapotranspiración"))
                            {
                                var datos = from i in context.DatosEquipos
                                            where i.IdVariablesXEquipos == idVarXEq &&
                                            i.Fecha >= stYest && i.Fecha < stToday
                                            select i;
                                if (datos.Count() == 0)
                                {
                                    sens.valor = "[]";
                                }
                                else
                                {
                                    DatosEquipos dat = datos.First();
                                    fechas.Add(dat.Fecha);
                                    if (dat.Valor <= -999.9)
                                        sens.valor = "-";
                                    else
                                        sens.valor = dat.Valor.ToString(numFormat);
                                }
                            }
                            else
                            {

                                //Find all instantaneous data for this Sensor
                                var datos = from i in context.DatosInstantaneos
                                            where i.IdVariablesXEquipos == idVarXEq
                                            select i;
                                if (datos.Count() == 0)
                                {
                                    sens.valor = "[]";
                                }
                                else
                                {
                                    DatosInstantaneo dat = datos.First();
                                    //add to list of dates
                                    fechas.Add(Convert.ToDateTime(dat.Fecha)); //_Actualizacion
                                    //if wind direction, change degrees to N,S,E,O letters
                                    if (var.Nombre.Contains("Dirección") && var.Nombre.Contains("Viento"))
                                        sens.valor = GetWindDirLetters(dat.Valor);
                                    else
                                    {
                                        if (dat.Valor <= -999.9)
                                            sens.valor = "-";
                                        else
                                            sens.valor = dat.Valor.ToString(numFormat);
                                    }

                                     if (sens.nombreSensor.Contains("Humedad de Aire exterior"))
                                      {
                                          humedad = true;
                                      }
                                      else if (sens.nombreSensor.Contains("Temperatura de Aire exterior"))
                                      {
                                          temperatura = true;
                                      }
                                }

                            }
                        }
                        sensores.Add(sens);
                    }

                    if (humedad && temperatura)
                    {
                        try
                        {
                            var salida = context.P2_VariablesCalculadas_DatosInstantaneos((int?)idEquipo);

                            foreach (P2_VariablesCalculadas_DatosInstantaneos_Result item in salida)
                            {
                                SensoresDatos sens = new SensoresDatos();
                                sens.nombreSensor = item.Variables;
                                sens.unidad = "°C";

                                if (sens.nombreSensor.Contains("Punto de Rocío"))
                                    sens.Icono = 25;
                                else if (sens.nombreSensor.Contains("Sensación Térmica"))
                                    sens.Icono = 29;
                                else //Temp de Bulbo Húmedo
                                    sens.Icono = 30;

                                sens.valor = item.Valor.ToString();  //sacar los ºC que devuelve
                                sens.valor = Regex.Replace(sens.valor, "[^0-9.-]", ""); //todo lo que no sea 0-9.- lo cambia por "" MAC2019 se agrega- para negativos
                                ////to-do Check for alarm
                                sens.alarma = false;
                                if (sens.valor.Length > 1)
                                    sensores.Add(sens);
                            }
                        }
                        catch (Exception e) { 
                        //...
                        }
                    }
                    //Get last date
                    //CultureInfo culture = new CultureInfo("es-AR");  
                    if (fechas.Count() > 0)
                        equip.fechaUltimaActualizacionDatos = fechas.OrderByDescending(x => x).First().ToString("dd/MM/yyyy HH:mm:ss");
                    else
                        equip.fechaUltimaActualizacionDatos = "[]";
                    equip.datosSensores = sensores;
                }
                var response = Request.CreateResponse();

                if (equip != null)
                    response.Content = new StringContent(JsonConvert.SerializeObject(equip), Encoding.UTF8, "application/json");
                else
                    response.Content = new StringContent("[]");
                return response;
                
            }

            }
            else
            {
                var response = Request.CreateResponse();
                JsonMsg expiredMsg = new JsonMsg();
                expiredMsg.msg = "Sesion de usuario expirada, por favor inicie la sesión nuevamente.";
                expiredMsg.accion = "reconectar";
                response.Content = new StringContent(JsonConvert.SerializeObject(expiredMsg), Encoding.UTF8, "application/json");
                return response;

            }
        }

        private string GetWindDirLetters(double windDirAngle)
        {
            string windDir = "";
            if (windDirAngle >= 348.75 || windDirAngle < 11.25)
                windDir = "N";
            else if (windDirAngle >= 11.25 && windDirAngle < 33.75)
                windDir = "NNE";
            else if (windDirAngle >= 33.75 && windDirAngle < 56.25)
                windDir = "NE";
            else if (windDirAngle >= 56.25 && windDirAngle < 78.75)
                windDir = "ENE";
            else if (windDirAngle >= 78.75 && windDirAngle < 101.25)
                windDir = "E";
            else if (windDirAngle >= 101.25 && windDirAngle < 123.75)
                windDir = "ESE";
            else if (windDirAngle >= 123.75 && windDirAngle < 146.25)
                windDir = "SE";
            else if (windDirAngle >= 146.25 && windDirAngle < 168.75)
                windDir = "SSE";
            else if (windDirAngle >= 168.75 && windDirAngle < 191.25)
                windDir = "S";
            else if (windDirAngle >= 191.25 && windDirAngle < 213.75)
                windDir = "SSO";
            else if (windDirAngle >= 213.75 && windDirAngle < 236.25)
                windDir = "SO";
            else if (windDirAngle >= 236.25 && windDirAngle < 258.75)
                windDir = "OSO";
            else if (windDirAngle >= 258.75 && windDirAngle < 281.25)
                windDir = "O";
            else if (windDirAngle >= 281.25 && windDirAngle < 303.75)
                windDir = "ONO";
            else if (windDirAngle >= 303.75 && windDirAngle < 326.25)
                windDir = "NO";
            else if (windDirAngle >= 326.25 && windDirAngle < 348.75)
                windDir = "NNO";
            else
                windDir = "-";
            return windDir;
        }
                
    }
}
