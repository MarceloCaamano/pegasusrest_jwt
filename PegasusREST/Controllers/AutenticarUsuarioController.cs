﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PegasusREST.Models;
using System.Data.Entity.Core.Objects;
using System.Web;
using System.Xml;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.IdentityModel.Tokens;
using PegasusREST.Security;



namespace PegasusREST.Controllers
{
    public class User
    {
        public bool autenticado {get; set;}
        public Int64 idCliente {get; set;}
        public string novedades {get; set;}
        public string token { get; set; }
    }
    /// <summary>
    /// login controller class for authenticate users
    /// </summary>
    [AllowAnonymous]
    public class AutenticarUsuarioController : ApiController
    {

        // GET api/test
       
        public HttpResponseMessage Post()
        {
            //#var validUserCookie = new HttpCookie("validUserCookie", null);
            using (PegasusClimaEntities context = new PegasusClimaEntities())
            {
                User currentUser = new User();
                var stream = HttpContext.Current.Request.InputStream;
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                string JSONbody = Encoding.UTF8.GetString(buffer);
                var parseTree = JsonConvert.DeserializeObject<JObject>(JSONbody);
                string nombreDeUsuario = "";
                string clave = "";
                
                foreach (var prop in parseTree.Properties())
                {
                    switch (prop.Name)
                    {
                        case "nombreDeUsuario":
                            nombreDeUsuario = prop.Value.ToString();
                            break;
                        case "clave":
                            clave = prop.Value.ToString();
                            break;
                    }
                }

                //find user that matches username from Usario table
                var userQuery = from u in context.Usuarios
                                where u.UserName == nombreDeUsuario
                                select u;
                if (userQuery.Count() > 0)
                {
                    Usuario user = userQuery.First();

                    //if password is correct, autenticado = true
                    if (user.Contrasenia == clave)
                    {

                        //#validUserCookie.Value = "valid";
                        //#validUserCookie.Expires = DateTime.Now.AddMinutes(5);
                        var token = TokenGenerator.GenerateTokenJwt(user.Id);//,"");
						//-var response= Request.CreateResponse(HttpStatusCode.OK, token, Configuration.Formatters.JsonFormatter);
                        //return Ok(token);
                        currentUser.autenticado = true;
                        currentUser.idCliente = user.Id;
                        currentUser.novedades = ""; //???? WHAT DOES THIS RELATE TO IN DATABASE?
                        currentUser.token = token;
                        //response.Content = new StringContent(JsonConvert.SerializeObject(currentUser), Encoding.UTF8, "application/json");
                        //return response;
                    }
                    else
                    {
                        //#currentUser.autenticado = false;
                        //#validUserCookie.Expires = DateTime.Now.AddDays(-1);
                    }

                    //#currentUser.idCliente = user.Id; 
                    //#currentUser.novedades = ""; //???? WHAT DOES THIS RELATE TO IN DATABASE?
                    
                }
                else
                {
                    //#currentUser.autenticado = false;
                    //#currentUser.idCliente = -1;
                    //#currentUser.novedades = "";
                    //#validUserCookie.Expires = DateTime.Now.AddDays(-1);
                    currentUser.autenticado = false;
                    currentUser.idCliente = -1;
                    currentUser.novedades = "";
                    currentUser.token = "";
                }

                //var respose= Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid User", Configuration.Formatters.JsonFormatter);
				//return Unauthorized();
                //# HttpContext.Current.Response.SetCookie(validUserCookie); 
                var respose = Request.CreateResponse();
                respose.Content=new StringContent(JsonConvert.SerializeObject(currentUser),Encoding.UTF8,"application/json");
                return respose;
                //return "test";                

            }
        }
    }
}
