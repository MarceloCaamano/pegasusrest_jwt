# Ejemplo de consulta a la API de TECMES/PEGASUS
# 
# Marcelo Caamaño - marcelo.caamano@tecmes.com
#
# History:
# 2015-07-10: created	

import requests, json


climagro_url="http://10.0.10.76:8080/api/"

#the queries
autenticar="AutenticarUsuario"
recuperarequipos="RecuperarEquipos"
instantaneo="RecuperarInstantaneosDeEquipo"

'''utilizo el objeto Session porque mantinene ciertos parámetros a
   traves de multiples peticiones (ej. las cookies) sirve para mantenerme
   logueado en la api
   '''
#M=requests.Session()

print("log in \n")
data1=json.dumps({'nombreDeUsuario':'demo', 'clave':'demo'})
r = requests.post(climagro_url+autenticar, data = data1)
print(r.text)

token=r.json()['token']
#token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZENsaWVudGUiOiIxNjg0OSIsIm5iZiI6MTYxNTkyMTE1MiwiZXhwIjoxNjE1OTIxMjEyLCJpYXQiOjE2MTU5MjExNTIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDkzNDYvIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo0OTM0NiJ9.kLGuWKDRdV264xzwt3TzpkD2Dw1Y7OIi0Vvg75o-8-8"

headers = { 'Authorization': 'Bearer {}'.format(token) }

print("\nrecuperar lista de equipos \n")
#data1=json.dumps({'idCliente':r.json()['idCliente']})
data1=json.dumps({'idCliente':16849})
r = requests.post(climagro_url+recuperarequipos, data=data1, headers=headers)
if r.ok:
    print(json.dumps(r.json(),indent=2))
else:
    print ("error ",r.status_code)

print("\nrecuperar instantaneos de equipos \n")
data2=json.dumps({'idEquipo':3})
r = requests.post(climagro_url+instantaneo, data = data2, headers=headers)
if r.ok:
    print(json.dumps(r.json(),indent=2))
else:
    print ("error ",r.status_code)

#M.close()
